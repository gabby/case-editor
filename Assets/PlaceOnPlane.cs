using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

    
[RequireComponent(typeof(ARRaycastManager))]
public class PlaceOnPlane : MonoBehaviour
{

    [SerializeField] private UnityEvent OnNotSupport;
    [SerializeField] private UnityEvent OnPlaneFound;
    public Transform obj3d;
    public GameObject spawnedObject { get; private set; }
    public GameObject m_PlacedPrefab;
    void Awake()
    {
        m_RaycastManager = GetComponent<ARRaycastManager>();
        //obj3d.transform.localScale = Vector3.zero;

    }
    IEnumerator Start()
    {


        if ((ARSession.state == ARSessionState.None) ||
            (ARSession.state == ARSessionState.CheckingAvailability))
        {
            yield return ARSession.CheckAvailability();
        }

        if (ARSession.state == ARSessionState.Unsupported)
        {
            // Start some fallback experience for unsupported devices
            print("[AR] OnNotSupport");
            OnNotSupport.Invoke();
        }
        else
        {
            // Start the AR session
        }
    }

    bool TryGetTouchPosition(out Vector2 touchPosition)
    {
        if (Input.touchCount > 0)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }

        touchPosition = default;
        return false;
    }
    void _Update()
    {
        if (!TryGetTouchPosition(out Vector2 touchPosition))
            return;

        if (m_RaycastManager.Raycast(touchPosition, s_Hits, TrackableType.PlaneWithinPolygon))
        {
            // Raycast hits are sorted by distance, so the first one
            // will be the closest hit.
            var hitPose = s_Hits[0].pose;

            obj3d.gameObject.SetActive(true);
            obj3d.position = hitPose.position;
            obj3d.rotation = hitPose.rotation;
        }
    }

    bool isObjSeted = false;
    bool isPlaneFound = false;
    float timeFoundPlane = 0;

    void Update()
    {
        if (isObjSeted) return;
        //if (!TryGetTouchPosition(out Vector2 touchPosition))
        //return;
        var touchPosition = new Vector2(Screen.width / 2f, Screen.height / 3);
        if (m_RaycastManager.Raycast(touchPosition, s_Hits, TrackableType.PlaneWithinPolygon))
        {
            if (!isPlaneFound)
            {
                isPlaneFound = true;
                timeFoundPlane = Time.time; //1
            }
            if(isPlaneFound && timeFoundPlane < Time.time - 1)
            {
                isObjSeted = true;
                var hitPose = s_Hits[0].pose;

                //obj3d.transform.localScale = Vector3.one;
                obj3d.position = hitPose.position;
                obj3d.rotation = hitPose.rotation;
                OnPlaneFound.Invoke();
            }

        }
    }

    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    ARRaycastManager m_RaycastManager;
}
