using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatorX : MonoBehaviour
{
    [SerializeField] private float minAngle = -90;
    [SerializeField] private float maxAngle = 90;
    [SerializeField] private float speed = 1;
    [SerializeField] private Transform target;

    Vector3 baseAng = Vector3.right;
    float ang = 0;
    private void Update()
    {
        if(Input.touchCount > 0)
        {
            
            ang += Input.touches[0].deltaPosition.y / (float)Screen.height * speed;
            if(ang < minAngle)
            {
                ang = minAngle;
            }
            if (ang > maxAngle)
            {
                ang = maxAngle;
            }print(ang);
            //ang = Mathf.Max(Mathf.Min(ang, maxAngle), minAngle);
            target.localEulerAngles = baseAng * ang;
        }
    }
}
