using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Model
{
    public static void SetAsFavorite(string sku, bool isFav)
    {
        PlayerPrefs.SetInt("favorites_" + sku, isFav ? 1 : 0);
    }

    public static bool IsFavorited(string sku)
    {
        return PlayerPrefs.GetInt("favorites_"+sku, 0) == 1;
    }
    
    public static bool CurModeIsPhone
    {
        get => PlayerPrefs.GetString("mode", "phone") == "phone";
        set => PlayerPrefs.SetString("mode", value ? "phone" : "book");
    }
    
    public static string LastOpenedPhone
    {
        get => PlayerPrefs.GetString("last_opened_phone", "");
        set => PlayerPrefs.SetString("last_opened_phone", value);
    }
    public static string LastOpenedBook
    {
        get => PlayerPrefs.GetString("last_opened_book", "");
        set => PlayerPrefs.SetString("last_opened_book", value);
    }
}
