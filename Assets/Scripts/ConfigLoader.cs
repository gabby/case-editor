using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Networking;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class ConfigLoader : MonoBehaviour
{
    public static ConfigLoader Instance;
    //public UnityEvent OnGetData;
    //public StringUnityEvent OnError;
    public static string mainDataUrl = "https://macetui.pl/app/";
    public static string phonesUrlBig = "";
    public static string booksUrlBig = "";
    public static string defPhone = "";
    public static string defBook = "";
    public static string phonesUrlSmall = "";
    public static string booksUrlSmall = "";
    
    private Dictionary<string, object> configDict;
    void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {

        StartCoroutine(GetData());
    }

    IEnumerator GetData()
    {
        UnityWebRequest www = UnityWebRequest.Get(mainDataUrl + "config.json?r=" + UnityEngine.Random.Range(22222, 99999));
        yield return www.SendWebRequest();
        //        state.text = "main config loaded";
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            //OnError.Invoke(www.error);
            yield break;
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            //PlayerPrefs.SetString("config", www.downloadHandler.text);
            configDict = Json.Deserialize(www.downloadHandler.text) as Dictionary<string, object>;
            var config = configDict["config"] as Dictionary<string, object>;
            phonesUrlSmall = config["phones_url_small"].ToString();
            phonesUrlBig = config["phones_url_big"].ToString();
            booksUrlSmall = config["books_url_small"].ToString();
            booksUrlBig = config["books_url_big"].ToString();
            defPhone = config["def_phone"].ToString();
            defBook = config["def_book"].ToString();

            //List<object> phones = (List<object>)configDict["phones"];
            //List<object> macbooks = (List<object>)configDict["macbooks"];
            //OnGetData.Invoke();
            UIController.instance.StartLoadLastOrFirst();
        }
    }
    public string GetUrlbig(string sku)
    {
        var categories = Model.CurModeIsPhone ? GetPhonesGallery() : getMacBooksGallery();
        print("sku:" + sku);
        foreach (var dataItem in categories)
        {
            Dictionary<string, object> cat = dataItem as Dictionary<string, object>;
            List<object> designs = cat["designs"] as List<object>;
            foreach (var item in designs)
            {
                var itemData = item as Dictionary<string, object>;
                print(itemData["sku"].ToString());
                print(itemData["big"].ToString());
                if (sku == itemData["sku"].ToString())
                {
                    return itemData["big"].ToString();
                }
            }
        }
        return "";
    }
    public string GetShopUrl(string sku)
    {
        var categories = Model.CurModeIsPhone ? GetPhonesGallery() : getMacBooksGallery();
        print("sku:" + sku);
        foreach (var dataItem in categories)
        {
            Dictionary<string, object> cat = dataItem as Dictionary<string, object>;
            List<object> designs = cat["designs"] as List<object>;
            foreach (var item in designs)
            {
                var itemData = item as Dictionary<string, object>;
                print(itemData["sku"].ToString());
                print(itemData["big"].ToString());
                if (sku == itemData["sku"].ToString())
                {
                    return itemData["product"].ToString();
                }
            }
        }
        return "";
    }
    //public string GetBigUrlBySku
    public List<object> GetPhonesGallery()
    {
        return (List<object>)configDict["phones"];
    }
    public List<object> getMacBooksGallery()
    {
        return (List<object>)configDict["macbooks"];
    }
}
/** /
{
	"config":
	{
		"phones_url_big":"phones/big/",
		"phones_url_small":"phones/small/",
		"books_url_big":"books/big/",
		"books_url_small":"books/small/"
	},
	"phones":
	[
		{
			"cat_name":"All",
			"designs":
			[
				{
					"sku" : "P0147",
					"big":"P0147.png",
					"small":"P0147.png",
					"product":"https://etsy.com"
				},{
					"sku" : "doom",
					"big":"doom.jpg",
					"small":"doom.jpg",
					"product":"https://etsy.com"
				},{
					"sku" : "1906720776_31105",
					"big":"1906720776_31105.png",
					"small":"1906720776_31105.png",
					"product":"https://etsy.com"
				},
			]
		}
	],
	"macbooks":
	[
		{
			
			"cat_name":"All",
			"designs":
			[
				{
					
				}
			]
		}
	],
/**/