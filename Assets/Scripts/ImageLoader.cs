﻿using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Linq;

[RequireComponent(typeof(Image))]
public class ImageLoader : MonoBehaviour
{
    //    private string defImage = "https://dapp.dblog.org/img/default.jpg";

    //    [SerializeField] private GameObject LoaderPrefab;
    //    [SerializeField] 
    private bool isLoading = false;
    private bool preserveAspect = false;
    private GameObject loader;
    // [HideInInspector] 
    public Image image;

    public Action OnLoaded;
    //    public int hashedImages = 0;

    private string plPrefsPrefix = "image_";

    void Awake()
    {

        Init();
    }

    private bool isInited = false;

    private void OnEnable()
    {
        if (isLoading && curUrl != "")
        {
            LoadImage(curUrl);
        }
    }

    private void Init()
    {
        if (isInited) return;
        //===todo fix "?" problem on device
        //        useCache = false;
        //=====

        image = GetComponent<Image>();
        image.sprite = null;
        image.enabled = false;
        isInited = true;
    }


    private string curUrl = "";
    //    private bool isVideo = false;

    
    
    private RectTransform _rectTransform;
    public RectTransform rectTransform{

        get{
            if (_rectTransform == null) _rectTransform = GetComponent<RectTransform>();
            return _rectTransform;
        }
    }
    public void LoadImage(string url, bool forceReload = false, bool isMuted = true)
    {
        if (forceReload)
        {
            curUrl = "";
            image.sprite = null;
        }
        isLoading = false;
        if (url == null) url = "";
        Init();
        image.enabled = false;
//        isVideo = false;
        
       

        
        if (url == "")
        {
            SetSprite(UIController.instance.DefImage);
            return;
        }

        if (image.sprite != null && curUrl == url)
        {
            image.enabled = true;
            return;
        }
        
        curUrl = url;
        image.sprite = null;
        
        
        //if(useCache && PlayerPrefs.HasKey(plPrefsPrefix + url))
        //{
        //    Debug.Log("[IMAGE_LOADER] Load from disk "+LoadFilePath(url));
        //    StartCoroutine(LoadFromDisk(url));
        //}
        //else
        {
            //Debug.Log("[IMAGE_LOADER] Load from url "+url);
            if(this.isActiveAndEnabled)
                StartCoroutine(DownloadImage(url));
            else
            {
                isLoading = true;
            }
        }
        
    }

    void SetSprite(Sprite sprite)
    {
        image.enabled = true;
        image.sprite = sprite;
        image.preserveAspect = preserveAspect;
    }
//    IEnumerator LoadFromDisk(string url)
//    {
////        if(loader == null)
////            loader = Instantiate(UIController.instance.ImageLoaderPrefab, transform);

////        yield return new WaitForSeconds(.3f);
//        // Start a download of the given URL
//        var www = new WWW(LoadFilePath(url));
//        yield return www;
////        Destroy(loader);
//        Texture2D texture = www.texture;
       
//        www.LoadImageIntoTexture(texture);
        
//        Rect rec = new Rect(0, 0, texture.width, texture.height);
////        image.color = new Color(1,1,1,0);
//        Sprite sprite =  Sprite.Create(texture, rec, new Vector2(0.5f,0.5f), 100);
////        image.sprite = sprite;
////        image.enabled = true;

//        SetSprite(sprite);
////        image.DOColor(Color.white, 1);

//    }
    
    IEnumerator DownloadImage(string url)
    {   
        if(loader == null)
            loader = Instantiate(UIController.instance.ImageLoaderPrefab, transform);
        isLoading = true;
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);
        yield return request.SendWebRequest();
        isLoading = false;
        Destroy(loader);
        
        if(request.isNetworkError || request.isHttpError) 
            Debug.Log(request.error);
        else
        {
            Texture2D texture = ((DownloadHandlerTexture) request.downloadHandler).texture;
            Rect rec = new Rect(0, 0, texture.width, texture.height);
            SetSprite(Sprite.Create(texture,rec,new Vector2(0.5f,0.5f),100));
            
        }
    }

    //private void SaveTextureToFile(Texture2D texture, string  fileName)
    //{
    //    string filePath = SaveFilePath(fileName);
       
    //    Debug.Log("[IMAGE_LOADER] save image "+filePath );

    //    byte[] itemBGBytes = texture.EncodeToJPG();
    //    File.WriteAllBytes(filePath, itemBGBytes);
    //}
    //private string LoadFilePath(string urlFileName)
    //{
    //    string filePath = "";
    //    if (Application.platform == RuntimePlatform.IPhonePlayer)
    //    {
    //        filePath = Path.Combine("file://" + Application.streamingAssetsPath, Server.MD5Hash(urlFileName) + ".jpg");
    //    }
    //    else if (Application.platform == RuntimePlatform.Android)
    //    {
    //        filePath = Path.Combine("jar:file://" + Application.streamingAssetsPath + "!/assets/", Server.MD5Hash(urlFileName) + ".jpg");
    //    }
    //    else
    //    {
    //        filePath = Path.Combine("file://" + Application.streamingAssetsPath, Server.MD5Hash(urlFileName) + ".jpg");
    //    }

    //    return filePath ;
    //}

    public void Reset()
    {
        image.sprite = null;
        curUrl = "";
    }
}
