using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.ARFoundation;

public class ARModeController : MouseToTouches
{
    //[SerializeField] private GameObject previewUI;
    //[SerializeField] private GameObject uiPreviewPhone;
    //[SerializeField] private GameObject uiPreviewBook;

    //[SerializeField] private TextureLoader uiPreviewPhoneRenderer;
    //[SerializeField] private TextureLoader uiPreviewBookRenderer;
    [SerializeField] private TextureLoader textureLoader;
    [SerializeField] private GameObject phone;
    [SerializeField] private GameObject book;

    [SerializeField] private GameObject arUI;
    [SerializeField] private Camera camera;
    [SerializeField] private GameObject objContainer;

    public void ShowHideArUI()
    {
        arUI.SetActive(!arUI.activeSelf);
    }
    public void Show3dObj()
    {
        Invoke("DaleyShow",1f);
    }
    private void DaleyShow()
    {
        phone.SetActive(Model.CurModeIsPhone);
        book.SetActive(!Model.CurModeIsPhone);
    }
    private void Start()
    {
        Screen.orientation = ScreenOrientation.AutoRotation;
        phone.SetActive(false);
        book.SetActive(false);

        if (Model.CurModeIsPhone)
        {
            //phone.SetActive(true);
            //book.SetActive(false);
            var big = ConfigLoader.Instance.GetUrlbig(Model.LastOpenedPhone);
            textureLoader.LoadPhoneTexture(ConfigLoader.mainDataUrl + ConfigLoader.phonesUrlBig + big);
            //uiPreviewPhoneRenderer.material.mainTexture = Image
        }
        else
        {
            //phone.SetActive(false);
            //book.SetActive(true);
            var big = ConfigLoader.Instance.GetUrlbig(Model.LastOpenedBook);
            textureLoader.LoadBookTexture(ConfigLoader.mainDataUrl + ConfigLoader.booksUrlBig + big);
        }
    }
   
    public void BackToMainScene()
    {
        UIController.instance.CloseArMode();
    }
    bool isWasDraged = false;
    //float prevZoom = 0;
    private float touchesSensivety = .3f;
    private float scaleSensivety = 1;
    public Transform MoveHelpPoint;
    private void Update()
    {
        //move
        if (Input.touchCount == 1)
        {
            var dist = Vector3.Distance(camera.transform.position, objContainer.transform.position);

            var deltaUnits = new Vector3(Input.touches[0].deltaPosition.x / (float)Screen.width, 0, (Input.touches[0].deltaPosition.y / (float)Screen.height) * 5f);

            var prevHelpPos = MoveHelpPoint.position;
            MoveHelpPoint.localPosition += deltaUnits * dist * touchesSensivety;// (touchesSensivety / ) * DataHolder.Instance.ObjSize;
            var helpDelta = MoveHelpPoint.position - prevHelpPos;

            objContainer.transform.position += new Vector3(helpDelta.x, 0, helpDelta.z);

            
        }
        else if (IsMultiTouch())
        {
            /*scale* /
            objInstance.transform.localScale += new Vector3(1,1,1) * ZoomingDistance * scaleSensivety;
            /**/

            //rotate
                DetectTouchMovement.Calculate();
                if (Mathf.Abs(DetectTouchMovement.turnAngleDelta) > 0)
                {
                    Vector3 rotationDeg = Vector3.zero;
                    rotationDeg.y = -DetectTouchMovement.turnAngleDelta;
                    objContainer.transform.localEulerAngles += rotationDeg;
                }
        }
    }
    public void OpenShop()
    {
        UIController.instance.OpenShop();
    }
}
