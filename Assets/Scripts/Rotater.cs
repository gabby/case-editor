﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotater : MonoBehaviour
{
    private float mouseSpeedMultiplier = 2000;
    private float smoothSpeed = 0.01f;
    private float mouseX;
    private float minSpeed = 0;
    private float curMinSpeed = 30;
    [SerializeField] private Transform target;

    void OnMouseDown() { 
        print("OnMouseDown");

    }
    void OnMouseDrag()
     {
        print("OnMouseDrag");
        mouseX += Input.GetAxis("Mouse X")/(float)Screen.width * mouseSpeedMultiplier;
     }
    bool isMouseDown;
    Vector3 startMousePosition;
    Vector3 startMousePosition2;
    Vector3 deltaMousePosition;

    void Start(){


        curPos = target.localEulerAngles;

    }
    private void OnEnable()
    {
        isMouseDown = false;
    }
    Vector3 rotateTarget;
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isMouseDown = true;
            startMousePosition = Input.mousePosition;// / (float)Screen.width;
            startMousePosition2 = Input.mousePosition;
            deltaMousePosition = (Input.mousePosition - startMousePosition) / (float)Screen.width;
            mouseX += deltaMousePosition.x * mouseSpeedMultiplier;
            // rotateTarget = new Vector3(0, -mouseX, 0);
            rotateTarget = curPos;
        }
        deltaMousePosition = (Input.mousePosition - startMousePosition) / (float)Screen.width;

        if (isMouseDown && Input.GetMouseButtonUp(0))
        {
            isMouseDown = false;
           if(Mathf.Abs(Input.mousePosition.x/(float)Screen.width  - startMousePosition2.x/(float)Screen.width ) < .001){

                curMinSpeed = 0;
                rotateTarget = curPos;
            }else{
                
                curMinSpeed = minSpeed;
           }
            drection = (rotateTarget.y > curPos.y) ? 1 : -1;
        }
        if(isMouseDown)
        {
            mouseX += deltaMousePosition.x * mouseSpeedMultiplier;
            rotateTarget = new Vector3(0, -mouseX, 0);
            startMousePosition = Input.mousePosition;
            float multDrag = 150000f;
            target.localEulerAngles += new Vector3(0, - deltaMousePosition.x / (float)Screen.width,0) * multDrag;// * Time.deltaTime;
            curPos += new Vector3(0, - deltaMousePosition.x/ (float)Screen.width,0) * multDrag;
        }
            // / (float)Screen.width;
        if(!isMouseDown && Mathf.Abs(curPos.y  - rotateTarget.y) > minSpeed )
        {
            
            curPos = Vector3.Lerp(curPos, rotateTarget, smoothSpeed);

            drection = (rotateTarget.y > curPos.y) ? 1 : -1;
        }
        

        //if(curMinSpeed != 0 && !isMouseDown && Mathf.Abs(curPos.y  - rotateTarget.y) < minSpeed){

        //    // mouseX += minSpeed * drection;
        //    // rotateTarget = new Vector3(0, -mouseX, 0);
        //    target.localEulerAngles += Vector3.up * minSpeed * drection * Time.deltaTime;
        //}else if(!isMouseDown){
        //    target.localEulerAngles = curPos;
        //}
    }
    float drection = 1;
    Vector3 curPos;

}
