using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextureLoader : MonoBehaviour
{
    [SerializeField] private Renderer rendererPhone;
    [SerializeField] private Renderer rendererBook;
    [SerializeField] private Text statusUI;

    public void LoadPhoneTexture(string url)
    {
        StartCoroutine(LoadingPhoneTexture(url));
    }
    public void LoadBookTexture(string url)
    {
        StartCoroutine(LoadingBookTexture(url));
    }

    IEnumerator LoadingPhoneTexture(string url)
    {
        print("[LOADING TEXTURE] " + url);
        WWW www = new WWW(url);
        while (www.progress < 1 && www.error == null)
        {
            if(statusUI != null) statusUI.text = "Loading... " + Mathf.RoundToInt(www.progress * 100f)+"%";//.ToString("0.0") + "%";
            yield return null;
            if (www.isDone) break;
        }
        //yield return www;
        if (statusUI != null) statusUI.text = "";

        if (www.error == null)
        {
            rendererPhone.material.mainTexture = www.texture;
        }
        else
        {
            Debug.Log("IMAGE LOADING ERROR");
        }
    }
    private string curUrl = "";
    IEnumerator LoadingBookTexture(string url)
    {
        if (curUrl == url) yield break;
        curUrl = url;
        rendererBook.material.mainTexture = null;
        print("[LOADING TEXTURE] " + url);
        WWW www = new WWW(url);
        while (www.progress < 1 && www.error == null)
        {
            if (statusUI != null) statusUI.text = "Loading... " + Mathf.RoundToInt(www.progress * 100f) + "%";//.ToString("0.0") + "%";
            yield return null;
            if (www.isDone) break;
        }
        //yield return www;
        if (statusUI != null) statusUI.text = "";
        if (www.error == null)
        {
            rendererBook.material.mainTexture = www.texture;

        }
        else
        {
            if (statusUI != null) statusUI.text = "IMAGE LOADING ERROR";
            Debug.Log("IMAGE LOADING ERROR");
        }
    }
}
