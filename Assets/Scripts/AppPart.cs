using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppPart : MonoBehaviour
{
    public static AppPart Instance;
    private void Awake()
    {
        if(Instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
    public void Show()
    {

    }
    public void Hide()
    {

    }
}
