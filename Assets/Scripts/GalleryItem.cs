using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor.Networking;
using System;

public class GalleryItem : MonoBehaviour
{
    public Action<Dictionary<string, object>> OnClick;
    public Action<Dictionary<string, object>, GalleryItem> OnFavClick;
    public Dictionary<string, object> data;
    public GameObject starOn;
    public Text designNumber;
    //public 
    public ImageLoader imageLoader;

    public Image image;
    //public bool isFavarited = false;

    public void OnBtnFavClick()
    {
        //isFavarited = !isFavarited;
        OnFavClick.Invoke(data, this);
    }

    public void OnBtnClick()
    {
        
        OnClick.Invoke(data);
    }
}
