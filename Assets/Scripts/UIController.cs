using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public static UIController instance;

    public ConfigLoader configLoader;
    public GalleryController galleryController;
    public GameObject ImageLoaderPrefab;
    public Sprite DefImage;
    public GalleryController gallery;
    public TextureLoader print3d;
    
    public Animator switcher;
    public UnityEvent onShowPhone;
    public UnityEvent onShowBook;
    public UnityEvent OnStart;
    public BoolUnityEvent OnFavorited;
    public UnityEvent OnShowAR;
    public UnityEvent OnHideAR;

    private void Awake()
    {
        instance = this;
        Application.targetFrameRate = 60;
    }
    private void Start()
    {
        OnStart.Invoke();
        switcher.SetBool("phone", Model.CurModeIsPhone);
    }

    public void StartLoadLastOrFirst() // call from config loader
    {
        if (Model.CurModeIsPhone)
        {
            SetPhoneTexture(Model.LastOpenedPhone == "" ? ConfigLoader.defPhone : Model.LastOpenedPhone);
        }
        else
        {
            SetBookTexture(Model.LastOpenedBook == "" ? ConfigLoader.defBook : Model.LastOpenedBook);
        }
        if (Model.CurModeIsPhone) onShowPhone.Invoke();
        if (!Model.CurModeIsPhone) onShowBook.Invoke();

    }
    public void Switch()
    {
        Model.CurModeIsPhone = !Model.CurModeIsPhone;
        switcher.SetBool("phone",Model.CurModeIsPhone);
        if (Model.CurModeIsPhone) onShowPhone.Invoke();
        if (!Model.CurModeIsPhone) onShowBook.Invoke();

        if (Model.CurModeIsPhone)
        {

            SetPhoneTexture(Model.LastOpenedPhone == "" ? ConfigLoader.defPhone : Model.LastOpenedPhone);
        }
        else
        {
            SetBookTexture(Model.LastOpenedBook == "" ? ConfigLoader.defBook : Model.LastOpenedBook);
        }
    }

    public void ShowGallery()
    {
        gallery.gameObject.SetActive(true);
        gallery.InitGallery(Model.CurModeIsPhone ? configLoader.GetPhonesGallery() : configLoader.getMacBooksGallery());
    }
    public void SetPhoneTexture(string sku, string bigUrl = "")
    {
        Model.LastOpenedPhone = sku;
        var big = bigUrl == "" ? ConfigLoader.Instance.GetUrlbig(sku) : bigUrl;
        print3d.LoadPhoneTexture(ConfigLoader.mainDataUrl + ConfigLoader.phonesUrlBig + big);
        OnFavorited.Invoke(Model.IsFavorited(sku));
    }
    public void SetBookTexture(string sku, string bigUrl = "")
    {
        Model.LastOpenedBook = sku;
        var big = bigUrl == "" ? ConfigLoader.Instance.GetUrlbig(sku) : bigUrl;
        print3d.LoadBookTexture(ConfigLoader.mainDataUrl + ConfigLoader.booksUrlBig + big);
        OnFavorited.Invoke(Model.IsFavorited(sku));
    }
    
    public void OnBookmarkClicked()
    {
        var sku = Model.CurModeIsPhone ? Model.LastOpenedPhone : Model.LastOpenedBook;
        Model.SetAsFavorite(sku, !Model.IsFavorited(sku));
        OnFavorited.Invoke(Model.IsFavorited(sku));
    }
    public void OpenArMode()
    {
        OnShowAR.Invoke();
        SceneManager.LoadScene(1, LoadSceneMode.Additive);
    }
    public void CloseArMode()
    {
        OnHideAR.Invoke();
        SceneManager.UnloadScene(1);

    }
    public void OpenShop()
    {
        Application.OpenURL(configLoader.GetShopUrl(Model.CurModeIsPhone ? Model.LastOpenedPhone : Model.LastOpenedBook));
    }
}
