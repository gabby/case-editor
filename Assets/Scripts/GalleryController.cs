using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GalleryController : MonoBehaviour
{
    
    [SerializeField] private GameObject phonePrefab;
    [SerializeField] private GameObject bookPrefab;
    [SerializeField] private RectTransform conteiner;
    [SerializeField] private GridLayoutGroup gridLayout;
    
    [SerializeField] private UnityEvent OnShow;
    [SerializeField] private UnityEvent OnHide;
    [SerializeField] private UnityEvent OnShowFavorites;
    [SerializeField] private UnityEvent OnHideFavorites;
    bool isInited = false;
    bool CurModeIsPhone = false;
    private List<GalleryItem> addedItems = new List<GalleryItem>();
    public void InitGallery(List<object> dataList)
    {
        isFavShowed = false;
        OnHideFavorites.Invoke();
        if (isInited && Model.CurModeIsPhone != CurModeIsPhone && addedItems.Count > 0)
        {
            foreach (var item in addedItems)
            {
                Destroy(item.gameObject);
            }
            addedItems.Clear();
        }
        else if (isInited && Model.CurModeIsPhone == CurModeIsPhone && addedItems.Count > 0)
        {
             return;
        }
        isInited = true;
        CurModeIsPhone = Model.CurModeIsPhone;
        //gridLayout. = Model.CurModeIsPhone ? 
        //var data = dataList
        foreach (var dataItem in dataList)
        {
            Dictionary<string, object> cat = dataItem as Dictionary<string, object>;
            gridLayout.cellSize = new Vector2(Model.CurModeIsPhone ? 230 : 360, Model.CurModeIsPhone ? 450 : 280);
            List<object> designs = cat["designs"] as List<object>;
            foreach (var item in designs)
            {
                var itemData = item as Dictionary<string, object>;
                GameObject itemGO = Instantiate(Model.CurModeIsPhone ? phonePrefab : bookPrefab, conteiner);
                GalleryItem galleryItem = itemGO.GetComponent<GalleryItem>();
                addedItems.Add(galleryItem);
                galleryItem.data = itemData;
                if (Model.IsFavorited(itemData["sku"].ToString()))
                {
                    galleryItem.starOn.SetActive(true);
                }
                galleryItem.OnClick = OnItemClick;
                galleryItem.OnFavClick = OnItemFavClick;
                galleryItem.imageLoader.LoadImage(
                    ConfigLoader.mainDataUrl + (Model.CurModeIsPhone ? ConfigLoader.phonesUrlSmall : ConfigLoader.booksUrlSmall) + itemData["small"].ToString());
            }

        }
        isInited = true;
    }
    private void OnItemFavClick(Dictionary<string, object> data, GalleryItem item)
    {
        bool val = !Model.IsFavorited(data["sku"].ToString());
        Model.SetAsFavorite(data["sku"].ToString(), val);
        item.starOn.SetActive(val);
    }
    private void OnItemClick(Dictionary<string, object> data)
    {
        print("[GALLRY]] set selected item " + data["big"]);
        if(Model.CurModeIsPhone)
            UIController.instance.SetPhoneTexture(data["sku"].ToString(), data["big"].ToString());
        else
            UIController.instance.SetBookTexture(data["sku"].ToString(), data["big"].ToString());
        gameObject.SetActive(false);
    }
    bool isFavShowed = false;
    public void OnShowHideFavClicked()
    {
        isFavShowed = !isFavShowed;

        if(isFavShowed)
            OnShowFavorites.Invoke();
        else
            OnHideFavorites.Invoke();

        foreach (var item in addedItems)
        {
            item.gameObject.SetActive(!isFavShowed || Model.IsFavorited(item.data["sku"].ToString()));
        }
    }

    private void OnDisable()
    {
        OnHide.Invoke();
    }
    private void OnEnable()
    {
        OnShow.Invoke();
    }
}
