using System;
using System.Collections;
using System.Collections.Generic;
// using System.IO;
using UnityEngine;

public class PhoneDesigns : MonoBehaviour
{
    [SerializeField] private int iterationIndex = 0;
    [SerializeField] private Sprite[] designs;
    [SerializeField] private GalleryItem[] items;
    private string imageFolder = "Phones";

    private void Awake()
    {
        Init();
    }

    private int designIndex = 0;
    [ContextMenu("Init")]
    private void Init()
    {
        StartCoroutine("RenderFlow");
    }

    IEnumerator RenderFlow()
    {
        iterationIndex = -1;
        while (designIndex < designs.Length)
        {
            iterationIndex++;
            
            int i = -1;
            foreach (var item in items)
            {
                i++;
                // string [] fileEntries = Directory.GetFiles(imageFolder);
                designIndex = i + items.Length * iterationIndex;
                if (designIndex < designs.Length)
                {
                    item.designNumber.text = designs[designIndex].name;
                    // item.imageLoader.LoadImage();
                    item.image.sprite = designs[designIndex];
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }

            yield return null;
            //save screen
            print("SAVE SCREEN "+iterationIndex);
        }
    }
}
